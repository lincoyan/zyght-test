<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    public function municipalities(){
        return $this->hasMany('App\\Municipality');
    }

    public function country(){
        return $this->belongsTo('App\\Country');
    }

}
