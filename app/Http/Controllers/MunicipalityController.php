<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Region;
use Illuminate\Validation\ValidationException;
use League\Flysystem\Exception;

class MunicipalityController extends Controller
{

    /**
     * Returns an array of municipalities found by its region id
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMunicipalitiesByRegion(Request $request){

        try{

            $this->validate($request, [
                'region_id' => 'required'
            ]);

            $region = Region::where('id', $request->get('region_id'))
                ->with('municipalities')
                ->first();

            if(!$region || !$region->municipalities){
                throw new \Exception("Datos no encontrados"); //TODO: Implement NotFoundEntityException
            }

            return response()->json([
                'message' => 'Datos encontrados',
                'result' => $region->municipalities
            ], 200);

        }catch(ValidationException $e){
            return response()->json([
                'message' => 'Error validando los datos',
                'result' => $e->errors()
            ], 400);
        }catch(\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
                'result' => []
            ], 404);
        }

    }
}
