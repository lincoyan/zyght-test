<?php

namespace App\Http\Controllers;
use App\Country;
use Illuminate\Http\Request;

class RegionController extends Controller
{

    /**
     * Returns an array of regions found by its country id
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRegionsByCountry(Request $request)
    {
        try {

            $this->validate($request, [
                'country_id' => 'required'
            ]);

            $country = Country::where('id', $request->get('country_id'))
                ->with('regions')
                ->first();
            if (!$country || !$country->regions) {
                throw new \Exception("Datos no encontrados"); //TODO: Implement NotFoundEntityException
            }

            return response()->json([
                'message' => 'Datos encontrados',
                'result' => $country->regions
            ], 200);

        } catch (ValidationException $e) {
            return response()->json([
                'message' => 'Error validando los datos',
                'result' => $e->errors()
            ], 400);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'result' => []
            ], 404);
        }


    }
}
