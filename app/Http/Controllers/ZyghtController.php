<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;

class ZyghtController extends Controller
{
    public function index(Request $request){

        $countries = Country::get();

        return view('index', [
            'countries' => $countries
        ]);

    }
}
