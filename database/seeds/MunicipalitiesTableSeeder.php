<?php

use Illuminate\Database\Seeder;

class MunicipalitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('municipalities')->insert([
            [
                'id' => 1,
                'name' => 'Quillota',
                'region_id' => 1
            ],
            [
                'id' => 2,
                'name' => 'Valparaiso',
                'region_id' => 1
            ],
            [
                'id' => 3,
                'name' => 'Providencia',
                'region_id' => 2
            ],
            [
                'id' => 4,
                'name' => 'Maipu',
                'region_id' => 2
            ],

        ]);
    }
}
