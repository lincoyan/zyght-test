<?php

use Illuminate\Database\Seeder;

class RegionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('regions')->insert([
            [
                'id' => 1,
                'name' => 'V Valparaiso',
                'country_id' => 1,
            ],
            [
                'id' => 2,
                'name' => 'Metropolitana',
                'country_id' => 1,
            ],
        ]);
    }
}
