<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/theme.min.css')}}">

    <title>Zyght - Test</title>
</head>
<body>

@include('navbar')

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <br>
            <form>
                <fieldset>

                    <legend>Selects anidados</legend>

                    <div class="form-group">
                        <label for="countries">Pais</label>
                        <select class="form-control" id="countries">
                            <option value="" selected>Seleccione pais</option>
                            @foreach($countries as $country)
                                <option value="{{$country->id}}">{{$country->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="regions">Region</label>
                        <select class="form-control" id="regions" disabled>
                            <option value="" selected>Seleccione region</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="municipalities">Comuna</label>
                        <select class="form-control" id="municipalities" disabled>
                            <option selected>Seleccione comuna</option>
                        </select>
                    </div>

                </fieldset>
            </form>

        </div>
    </div>

</div>

<script type="application/javascript">
    var baseUrl = '{{url("/")}}';
</script>

<script
        src="https://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
        crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

<script src="{{asset('js/index.js')}}"></script>
</body>
</html>