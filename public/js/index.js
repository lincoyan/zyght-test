/**
 * Created by lincoyan on 04-12-17.
 */

$(document).ready(function(){
    $('#countries').on('change', function(){
        populateRegions();
    });

    $('#regions').on('change', function(){
        populateMunicipalities();
    })
})


/**
 * Sets a CSRF token header in every AJAX request
 */
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

/**
 * Populates regions select with options depending on selected country
 *
 * @param {function} callback
 */
function populateRegions(callback){

    emptySelectsForRegions();

    if($('#countries').val() == ''){
        return;
    }

    var query = {
        country_id : $('#countries').val()
    };

    lockSelects();
    $.get(baseUrl + '/api/regions', query, function(data){
        if(data.result.length > 0){
            for (var i in data.result){
                var option = $('<option>' + data.result[i].name + '</option>');
                option.val(data.result[i].id);
                $('#regions').append(option);
            }
        }
    }).always(function(){
        unlockSelects();
    }).fail(function(res){
        alert(res.responseJSON.message); //TODO: implement a more friendly UI error component
    })

}

/**
 * Populates municipalities select with options depending on selected region
 *
 * @param {function} callback
 */
function populateMunicipalities(callback){

    emptySelectsForMunicipalities();

    if($('#regions').val() == ''){
        return;
    }

    var query = {
        region_id : $('#regions').val()
    };

    lockSelects();
    $.get(baseUrl + '/api/municipalities', query, function(data){
        if(data.result.length > 0){
            for (var i in data.result){
                var option = $('<option>' + data.result[i].name + '</option>');
                option.val(data.result[i].id);
                $('#municipalities').append(option);
            }
        }
    }).always(function(){
        unlockSelects();
    }).fail(function(res){
        alert(res.responseJSON.message); //TODO: implement a more friendly UI error component
    })

}

/**
 * Remove all Region and Municipality options and reset placeholders
 */
function emptySelectsForRegions(){

    var regionsPlaceholderOption = $('#regions option:first');
    $('#regions option').remove();
    $('#regions').append(regionsPlaceholderOption);

    emptySelectsForMunicipalities();

}

/**
 * Remove all Municipality options and reset placeholders
 */
function emptySelectsForMunicipalities(){
    var municipalitiesPlaceholderOption = $('#municipalities option:first');
    $('#municipalities option').remove();
    $('#municipalities').append(municipalitiesPlaceholderOption);
}

/**
 * Disables Region and Municipality selects
 */
function lockSelects(){
    $('#municipalities').prop('disabled', true);
    $('#regions').prop('disabled', true);
}

/**
 * Enables Region and Municipality selects
 */
function unlockSelects(){
    $('#municipalities').prop('disabled', false);
    $('#regions').prop('disabled', false);
}